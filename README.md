# README
== passionate_navigation / Api with auth token based


This README would normally document whatever steps are necessary to get the
application up and running.

Things you have to to cover:

* Ruby version - 2.6.3
* Rails version - 5.2.3

* Installation

- Install {Bundler}[http://bundler.io/] if you haven't already
- Database
  - database.yml configuration accordingly
  - Create database
- +$ bundle install+
- +$ rake db:setup+ OR +$ rake db:create && rake db:migrate && rake db:seed+
- +$ rails s+

* ...


== Api responses

* verticals api
1. index
  url - <End_point>/api/v1/verticals
  method - get
  * requirments
    X-Authorization - < encrepted secret_base_key >
  * response
    All vertival records in json formate

2. show
  url - <End_point>/api/v1/verticals/:id
  method - get
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
    params - id
  * response
    Vertival in json formate

3. create
  url - <End_point>/api/v1/verticals
  method - post
  * requirments
    X-Authorization - < encrepted secret_base_key >
    params - vertical[name]
  * response
    success true/false
4. update
  url - <End_point>/api/v1/verticals/:id
  method - patch
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
    params - id
    params - vertical[name]
  * response
    success true/false
5. delete
  url - <End_point>/api/v1/verticals/:id
  method - delete
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
    params - id
  * response
    success true


* categories api

1. index
  url - <End_point>/api/v1/categories
  method - get
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
  * response
    All category records in json formate

2. show
  url - <End_point>/api/v1/categories/:id
  method - get
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
    params - id
  * response
    category in json formate

3. create
  url - <End_point>/api/v1/categories
  method - post
  * requirments
    X-Authorization - < encrepted secret_base_key >
    params - category[name]
    params - category[state]
    params - category[vertical_id]
  * response
    success true/false
4. update
  url - <End_point>/api/v1/categories/:id
  method - patch
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
    params - id
    params - category[name]
    params - category[state]
    params - category[vertical_id]
  * response
    success true/false
5. delete
  url - <End_point>/api/v1/categories/:id
  method - delete
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
    params - id
  * response
    success true

* courses api

1. index
  url - <End_point>/api/v1/courses
  method - get
  * requirments
    X-Authorization - < encrepted secret_base_key >
  * response
    All course records in json formate

2. show
  url - <End_point>/api/v1/courses/:id
  method - get
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
    params - id
  * response
    course in json formate

3. create
  url - <End_point>/api/v1/courses
  method - post
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
    params - course[name]
    params - course[state]
    params - course[category_id]
    params - course[author]
  * response
    success true/false
4. update
  url - <End_point>/api/v1/courses/:id
  method - patch
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
    params - id
    params - course[name]
    params - course[state]
    params - course[category_id]
    params - course[author]
  * response
    success true/false
5. delete
  url - <End_point>/api/v1/courses/:id
  method - delete
  * requirments
    in header - X-Authorization: < encrepted secret_base_key >
    params - id
  * response
    success true