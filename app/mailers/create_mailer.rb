class CreateMailer < ApplicationMailer
  def send_mail(record)
    @record = record
    mail(to: 'test@gmail.com', subject: 'new record created')
  end
end