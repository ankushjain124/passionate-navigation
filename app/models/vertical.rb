class Vertical < ApplicationRecord
  validates :name, presence: true
  validate :check_uniqness

  has_many :categories
  after_create :send_email

  private

  def check_uniqness
    if Vertical.pluck(:name).include?(name) || Category.pluck(:name).include?(name)
      errors.add(:name, 'should be uniq across  category and vertical both models')
    end
  end

  def send_email
    CreateMailer.send_mail(self).deliver_now
  end
end
